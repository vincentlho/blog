from django.urls import path, re_path

from blog.views import post_list, post_detail, post_share


app_name='blog'

urlpatterns=[
    re_path(r'^post/list/$', post_list, name='post_list'),
    re_path(r'^tag/(?P<tag_slug>[-\w]+)/$', post_list, name='post_list_by_tag'),
    re_path(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<post>[-\w]+)/$', post_detail, name='post_detail'),
    re_path(r'^(?P<post_id>\d+)/share/$', post_share, name='post_share'),
]